import './App.css';
import React, {useState} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './components/Home';
import OnlineShop from './components/OnlineShop';
import  { Switch, Route, Redirect } from 'react-router-dom';

function App() {
  // const [lang, setLang] = useState('');
  // if(!localStorage.getItem("lang")){
  //   localStorage.setItem("lang", "UK");
  //   setLang(localStorage.getItem("lang"))
  // } else setLang(localStorage.getItem("lang"))

  return (
    <div className='App'>
      <Switch>
        <Route exact path='/'>
          <Redirect to='/home' />
        </Route>
        <Route exact path='/home' component={Home}/>
        <Route exact path='/shop' component={OnlineShop} />
        
      </Switch>
  

      
    </div>
  );
}

export default App;
