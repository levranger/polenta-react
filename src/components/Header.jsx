import React, { useState } from "react";
import PolentaLogo from "../shared/img/Frame.png";
import "./Header.css";

const Header = (props) => {
    console.log("HEADER render")
    // const [lang, setLang] = useState("");
    let {title} = props;



    return(
        <header class="header">
            <div class="container">
                <div class="row">

     
                        <p className="header-title">
                            {title}
                        </p>
              
                   
                        <div class="logo col-4 offset-md-4">
                            <img src={PolentaLogo} alt="logo"/>
                        </div>
                        <div className="language-buttons">
                          <span>UK</span> &nbsp; <span>RU</span> &nbsp; <span>EN</span>
                        </div>
                    
                
       

                </div>
            </div>
    </header>
    )
}

export default Header;