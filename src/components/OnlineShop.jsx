import React from "react";
import SideMenu from "./SideMenu";
import Header from "./Header";
import img from "../shared/img/polenta.png"
import "./OnlineShop.css";
const OnlineShop = () => {
    return(
        <div className="shop-page">
          
               <SideMenu/>
               <Header title="Онлайн магазин"/>
            <div className="first-section">
                <div className="container product-container">
                    <div className="row">
                        <div className="col-md-3 col-xs-10 product-card">
                                    <img src={img} alt="Polenta"/>
                                    <p className="product-title">Полента “Класична” <br/> Вага 500г</p>
                                    <p className="product-price">Ціна</p>
                                    <div className="price-rectangle"></div>   
                                
                        </div>
                    </div>
                </div>
            
            </div>        
        </div>
    )
}
 export default OnlineShop;