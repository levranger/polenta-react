import React, {Component} from "react";
import "./SideMenu.css";
import { slide as Menu } from 'react-burger-menu';
import { Link } from 'react-router-dom';

class SideMenu extends Component {
    constructor(props){
        super(props)
        this.state = {
          menuOpen:false,
        }
    }
  showSettings (event) {
    event.preventDefault();
  
  }
  
  handleStateChange (state) {
    this.setState({menuOpen:state.isOpen})
  }
  closeMenu () {
    this.setState({menuOpen:false})
  }
  toggleMenu () {
    this.setState(state=> ({menuOpen : !state.menuOpen}))
  }
  render () {
    // NOTE: You also need to provide styles, see https://github.com/negomi/react-burger-menu#styling
    return (
      <Menu isOpen={false} left onOpen={ this.handleOpen } width={ " 300px " } >
        <Link to="/home" id="home" className="menu-item">Головна сторінка</Link>
        <Link to="/shop" id="shop" className="menu-item" >Онлайн магазин</Link>
        <Link to="/contact" id="product" className="menu-item" >О продукте</Link>
        <a id="recepie" href="" className="menu-item">
          Рецепти
          <li>Нові рецепти</li>
          <li>Бібліотека рецептів</li>
        </a>
        <a href="contacts" className="menu-item" >
          Зворотній зв’язок
            <li>Поширені запитання</li>
            <li>Пропозиції та коментарі</li>
        </a>
        <a href="">Контакти</a>   
      </Menu>
    );
  }
}

export default SideMenu;
